package Webdrivercommand;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Mozilla
{
public static void main(String[] args) throws InterruptedException
{
System.setProperty("webdriver.gecko.driver","D:\\Software\\Driver\\geckodriver.exe");
WebDriver driver=new FirefoxDriver();
driver.get("https://www.google.com/");
String baseurl=driver.getCurrentUrl();
System.out.println(baseurl);
Thread.sleep(5000);
driver.navigate().to("https://seleniummaster.com/llc/");
String tourl=driver.getCurrentUrl();
System.out.println(tourl);
Thread.sleep(5000);
driver.navigate().back();
String backurl=driver.getCurrentUrl();
System.out.println(backurl);
Thread.sleep(5000);
driver.navigate().refresh();
String refreshurl=driver.getCurrentUrl();
System.out.println(refreshurl);
Thread.sleep(5000);
driver.navigate().to("https://seleniummaster.com/llc/");
String tourl1=driver.getCurrentUrl();
System.out.println(tourl1);
Thread.sleep(5000);
driver.manage().window().maximize();
Thread.sleep(5000);
driver.close();
}
}
